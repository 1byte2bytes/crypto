module gitlab.com/elixxir/crypto

go 1.13

require (
	github.com/golang/protobuf v1.4.1 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/errors v0.9.1
	github.com/spf13/jwalterweatherman v1.1.0
	gitlab.com/elixxir/primitives v0.0.0-20200515202141-16fa8236f167
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37
	google.golang.org/grpc v1.29.1
)
